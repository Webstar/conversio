const getInt = () => {
  return Math.random() * (9 - 2) + 0;
}

const getSortaRandBool = () => {
  const rand = Math.random() * (5 - 0) + 0;
  if (rand > 1){
    return false;
  }
  else {
    return true
  }
}

const genRandomArray = () => {
  let parent = [];
  for (let i=0; i<=getInt(); i++){
    if (getSortaRandBool()){
      parent.push(genRandomArray()); //recurision, call stack - but shouldn't be an issue here
    }
    else {
      parent.push(i);
    }
  }
  return parent;
}

const flatten = array => {
  let i = 0;
  while (i != array.length) {
      let valueOrArray = array[i];
      // Maybe check for cyclic ref...
      if (! Array.isArray(valueOrArray)) {
          i++;
      } else {
          array.splice(i, 1, ...valueOrArray);
      }
  }
  return array;
}

let a = genRandomArray();
console.log('Before:');
console.log(a);
console.log('\n');
console.log(`After:`);
console.log(flatten(a));